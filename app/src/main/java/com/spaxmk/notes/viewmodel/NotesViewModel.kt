/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.spaxmk.notes.local.NotesDb
import com.spaxmk.notes.model.Notes
import com.spaxmk.notes.repository.NotesRepository

class NotesViewModel(application: Application): AndroidViewModel(application) {
    val repository:NotesRepository

    init {
        val dao = NotesDb.getDbInstance(application).myNotesDao()
         repository = NotesRepository(dao)
    }


    fun getlowNotes():LiveData<List<Notes>> = repository.getlowNotes()

    fun  getmediumNotes():LiveData<List<Notes>> = repository.getmediumNotes()

    fun  gethighNotes():LiveData<List<Notes>> = repository.gethighNotes()


    fun addNotes(notes: Notes){
        repository.insertNotes(notes)
    }

    fun getNotes():LiveData<List<Notes>> = repository.getAllNotes()

    fun deleteNotes(id:Int){
        repository.deleteNotes(id)
    }

    fun  updateNotes(notes: Notes){
        repository.updateNotes(notes)
    }

}