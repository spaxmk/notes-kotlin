/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "Notes")
class Notes (
    @PrimaryKey(autoGenerate = true)
    var _id:Int?=null,
    var _title:String,
    var _subTitle: String,
    var _notes: String,
    var _date: String,
    var _priority: String
        ):Parcelable
