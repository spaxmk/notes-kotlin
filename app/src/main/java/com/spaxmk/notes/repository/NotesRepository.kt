/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.repository

import androidx.lifecycle.LiveData
import com.spaxmk.notes.local.NotesDao
import com.spaxmk.notes.model.Notes

class NotesRepository(val dao: NotesDao) {
    fun getAllNotes():LiveData<List<Notes>> {
        return dao.getNotes()
    }

    fun getlowNotes():LiveData<List<Notes>> = dao.getLowNotes()

    fun  getmediumNotes():LiveData<List<Notes>> = dao.getMediumNotes()

    fun  gethighNotes():LiveData<List<Notes>> = dao.getHighNotes()

    fun insertNotes(notes: Notes){
        dao.insertNotes(notes)
    }

    fun deleteNotes(id: Int){
        dao.deleteNotes(id)
    }

    fun updateNotes(notes: Notes){
        dao.updateNotes(notes)
    }

}