/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.ui.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.spaxmk.notes.R
import com.spaxmk.notes.databinding.ItemNotesBinding
import com.spaxmk.notes.model.Notes
import com.spaxmk.notes.ui.Fragments.HomeFragmentDirections

class NotesAdapter( val requireContext: Context, val notesList: List<Notes>) :
    RecyclerView.Adapter<NotesAdapter.notesViewHolder>() {

    class notesViewHolder(val binding: ItemNotesBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): notesViewHolder {
        return notesViewHolder(
            ItemNotesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: notesViewHolder, position: Int) {
        val data = notesList[position]
        holder.binding.notesTitle.text = data._title
        holder.binding.notesSubtitle.text = data._subTitle
        holder.binding.notesDate.text = data._date.toString()

        when(data._priority){
            "1"->{
                holder.binding.viewPriority.setBackgroundResource(R.drawable.green_dot)
            }
            "2"->{
                holder.binding.viewPriority.setBackgroundResource(R.drawable.yellow_dot)
            }
            "3"->{
                holder.binding.viewPriority.setBackgroundResource(R.drawable.red_dot)
            }

        }

        holder.binding.root.setOnClickListener{
            val action = HomeFragmentDirections.actionHomeFragment2ToEditNotesFragment2(data)
            Navigation.findNavController(it).navigate(action)
        }
    }

    override fun getItemCount() = notesList.size

}