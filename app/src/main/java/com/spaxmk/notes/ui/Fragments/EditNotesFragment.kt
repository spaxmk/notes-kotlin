/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.ui.Fragments

import android.os.Bundle
import android.text.format.DateFormat
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.spaxmk.notes.R
import com.spaxmk.notes.databinding.FragmentCreateNotesBinding
import com.spaxmk.notes.databinding.ItemNotesBinding
import com.spaxmk.notes.model.Notes
import com.spaxmk.notes.ui.Adapter.NotesAdapter
import com.spaxmk.notes.viewmodel.NotesViewModel
import java.util.*

class EditNotesFragment : Fragment() {

    val _note by navArgs<EditNotesFragmentArgs>()
    lateinit var binding: FragmentCreateNotesBinding
    var priority: String = "1"
    val viewModel: NotesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCreateNotesBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)


        binding.editTitle.setText(_note.data._title)
        binding.editSubTitle.setText(_note.data._subTitle)
        binding.editNotes.setText(_note.data._notes)

        when(_note.data._priority){
            "1"->{
                priority = "1"
                binding.pGreen.setImageResource(R.drawable.ic_baseline_done_24)
                binding.pRed.setImageResource(0)
                binding.pYellow.setImageResource(0)
            }
            "2"->{
                priority = "2"
                binding.pRed.setImageResource(R.drawable.ic_baseline_done_24)
                binding.pGreen.setImageResource(0)
                binding.pYellow.setImageResource(0)
            }
            "3"->{
                priority = "3"
                binding.pYellow.setImageResource(R.drawable.ic_baseline_done_24)
                binding.pRed.setImageResource(0)
                binding.pGreen.setImageResource(0)
            }

        }

        binding.pGreen.setOnClickListener{
            priority = "1"
            binding.pGreen.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pRed.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }
        binding.pRed.setOnClickListener{
            priority = "2"
            binding.pRed.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pGreen.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }
        binding.pYellow.setOnClickListener{
            priority = "3"
            binding.pYellow.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pRed.setImageResource(0)
            binding.pGreen.setImageResource(0)
        }

        binding.btnSaveNotes.setOnClickListener{
            updateNotes(it)
        }

        return binding.root
    }

    private fun updateNotes(it: View?) {
        val _title = binding.editTitle.text.toString()
        val _subTitle = binding.editSubTitle.text.toString()
        val _notes = binding.editNotes.text.toString()

        val date = Date()
        val _notesDate: CharSequence = DateFormat.format("MMMM d, yyyy", date.getTime())

        if(_title!=""&&_notes!=""){
            val notes = Notes(_note.data._id, _title, _subTitle, _notes, _notesDate.toString(), priority)
            viewModel.updateNotes(notes)
            Toast.makeText(requireActivity(), "Note Updated Successfully", Toast.LENGTH_LONG).show()
            Navigation.findNavController(it!!).navigate(R.id.action_editNotesFragment2_to_homeFragment2)
        }else{
            Toast.makeText(requireContext(), "Required - Title and Notes", Toast.LENGTH_LONG).show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId==R.id.menu_delete){
            val bottomSheet:BottomSheetDialog = BottomSheetDialog(requireContext())
            bottomSheet.setContentView(R.layout.dialog_delete)

            //val texviewYes = bottomSheet.findViewById<TextureView>(R.id.dialog_yes)
            //val texviewNo = bottomSheet.findViewById<TextureView>(R.id.dialog_no)

            //texviewYes?.setOnClickListener{
            //    viewModel.deleteNotes(_note.data._id!!)
            //    Navigation.findNavController(it!!).navigate(R.id.action_editNotesFragment2_to_homeFragment2)
            //}
            //texviewNo?.setOnClickListener{
            //    bottomSheet.dismiss()
            //}

            bottomSheet.show()

        }

        return super.onOptionsItemSelected(item)
    }
}