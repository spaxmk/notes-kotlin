package com.spaxmk.notes.ui.Fragments

import android.os.Bundle
import android.text.format.DateFormat
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.spaxmk.notes.R
import com.spaxmk.notes.databinding.FragmentCreateNotesBinding
import com.spaxmk.notes.model.Notes
import com.spaxmk.notes.viewmodel.NotesViewModel
import java.util.*


class CreateNotesFragment : Fragment() {

    lateinit var binding : FragmentCreateNotesBinding
    var priority: String = "1"
    val viewModel : NotesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCreateNotesBinding.inflate(layoutInflater, container, false)

        binding.btnSaveNotes.setOnClickListener{
            createNotes(it)
        }

        binding.pGreen.setImageResource(R.drawable.ic_baseline_done_24)

        binding.pGreen.setOnClickListener{
            priority = "1"
            binding.pGreen.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pRed.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }
        binding.pRed.setOnClickListener{
            priority = "2"
            binding.pRed.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pGreen.setImageResource(0)
            binding.pYellow.setImageResource(0)
        }
        binding.pYellow.setOnClickListener{
            priority = "3"
            binding.pYellow.setImageResource(R.drawable.ic_baseline_done_24)
            binding.pRed.setImageResource(0)
            binding.pGreen.setImageResource(0)
        }

        return binding.root
    }

    private fun createNotes(it: View?) {

        val _title = binding.editTitle.text.toString()
        val _subTitle = binding.editSubTitle.text.toString()
        val _notes = binding.editNotes.text.toString()

        val date = Date()
        val _notesDate: CharSequence = DateFormat.format("MMMM d, yyyy", date.getTime())

        if(_title!=""&&_notes!=""){
            val notes = Notes(null, _title, _subTitle, _notes, _notesDate.toString(), priority)
            viewModel.addNotes(notes)
            Toast.makeText(requireActivity(), "Successfully", Toast.LENGTH_LONG).show()
            Navigation.findNavController(it!!).navigate(R.id.action_createNotesFragment2_to_homeFragment2)
        }else{
            Toast.makeText(requireContext(), "Required - Title and Notes", Toast.LENGTH_LONG).show()
        }


    }

}