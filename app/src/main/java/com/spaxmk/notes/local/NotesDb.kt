/*
 * Copyright (c) Spasoja Stojsic - 2021
 */

package com.spaxmk.notes.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.spaxmk.notes.model.Notes

@Database(entities = [Notes::class], version = 1, exportSchema = false)
abstract class NotesDb : RoomDatabase() {
    abstract fun myNotesDao():NotesDao

    companion object{
        @Volatile
        var INSTANCE:NotesDb?=null

        fun getDbInstance(context: Context):NotesDb{
            val tempInstance = INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val  roomDatabaseInstance = Room.databaseBuilder(context, NotesDb::class.java, "NotesDb").allowMainThreadQueries().build()
                INSTANCE=roomDatabaseInstance
                return roomDatabaseInstance
            }
        }
    }


}